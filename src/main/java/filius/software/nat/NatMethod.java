package filius.software.nat;

public enum NatMethod {
	fullCone,
	restrictedCone
}
